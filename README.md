# README #

This package is exactly same as python pymongo package which can be installed by the command : pip install pymongo==3.8.0,
except for one feature , this package helps encrypt the fields which you can manually add add into package functions.


HOW TO INSTALL :

pip install git+ssh://git@bitbucket.org/Saksham73/pymongo_package.git

SETUP Instructions:

1) Files => collection.py, cursor.py have a statement 'from pymongopoc.keys import encrypted_dict'
	 -> encrypted_dict is supposed to reference a python dictionary which has Collection name as the key and a python list having that collection field names 
	 	that you wish to keep encrypted.
		Please declare it before using.

2) Files => collection.py, cursor.py and command_cursor.py have an import 'from encryption.AES import AESCipher':
	 -> This AESCipher is the class which is actually used to encrypt your data.
		which also uses a key and an initial vector to encrypt and decryopt your data.
	 -> generate the key and initial vector only once and save them securely in your project so that you can have your data decrypted to obtain correct information.
	 -> Some Commands for reference:
	 	# Generating encryption key -> encryption_key = os.urandom(16)
		# generating initial Vector -> AES_iv = hashlib.sha256(encryption_key).digest()
		
3) you can download the AES.py used along with this package from the below given link and place at appropriate position in your project according to import mentioned in package.
	LINK -> https://drive.google.com/file/d/1ijo9RFP8RiZMVj0EkGdVAaPaGd5ZGFmd/view?usp=sharing

WALKTHROUGH OF WHAT ALL HAS BEEN DONE:
1) Added a function 'find_fields_and_encrypt()' in collection.py-> it is supposed to take dictionary as input, which is 	given to any mongo query to act a as a find criteria if required. This function will find track of if any fields that 	  we keep encrypted are part of this dictionary and will encrypt them.

2)Added a function 'encrypt_in_inserted_docs()' in collection.py -> it is supposed to take dictionary or list of 			dictionary as input, which is given to any mongo query to insert them to form new records if required. This function 	 will find track of if any fields that we keep encrypted are part of this input and will encrypt them.

3) Added a function 'find_and_decrypt_for_agg_match_pipeline()' in collection.py -> this function captures the $match 		operator in aggregation pipeline and enrypt if any fields it finds which we need to keep in encrypted form.

4) for the cases when the query has been executed and will return documents as a result of any find query ot aggregation 	result,  we want the encrypted fields to be decrypted back to original data.
	this we have taken by wrapping our class 'Cursor' and 'CommandCurosr' defined in cursor.py and command_cursor.py in pymongo.
	These wrapper classes are defind with names as 'Class MyCursor' and 'Class MyCommandCursor' under the respective same files. they are responsible to act as a generator for the returned cursors and decrypt the information in those cursors.

