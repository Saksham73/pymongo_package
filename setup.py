from setuptools import setup, find_packages

setup(
    name='pymongo',
    version='3.8.0',
    description='Modified Pymongo Package To Be used by the internal team of ziploan ',
    author='Ziploan',
    author_email='',
    url="https://bitbucket.org/Saksham73/pymongo_package",
    packages=find_packages(),
    package_data={},
    include_package_data=True,
    install_requires=[],
    zip_safe=False
)
